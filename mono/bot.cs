using System;
using System.Net;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class Bot
{
    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        Console.Title = "Team Ezimr HWO 2014";

        HWO_Log.Log("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot(reader, writer, new Join(botName, botKey));
        }
        //Console.ReadKey();
    }

    private StreamWriter writer;

    int gameTick = 0;

    bool isTurbo = false;

    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        string line;
        bool turboAvailable = false;
        send(join);

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch (msg.msgType)
            {
                case "turboAvailable":
                    Console.Title = "TURBO AVAILABLE!!!";
                    turboAvailable = true;
                    JArray ta = new JArray(JsonConvert.DeserializeObject(line));
                    /*turboDur = int.Parse(ta[0]["data"]["turboDurationTicks"].ToString());
                    turboFac = double.Parse(ta[0]["data"]["turboFactor"].ToString());
                    turboEnd = gameTick + turboDur;*/
                    break;
                case "carPositions":
                    double throttle = 1.0;
                    double angle = 0.0;
                    JArray carPosition = new JArray(JsonConvert.DeserializeObject(line));

                    /*if(turboEnd >= gameTick)
                    {
                        throttle = (1/turboFac)*2;
                        break;
                    }*/

                    angle = double.Parse(carPosition[0]["data"][0]["angle"].ToString());

                    int currentLap = int.Parse(carPosition[0]["data"][0]["piecePosition"]["lap"].ToString());
                    int pieceIndex = int.Parse(carPosition[0]["data"][0]["piecePosition"]["pieceIndex"].ToString());
                    try
                    {
                        gameTick = int.Parse(carPosition[0]["gameTick"].ToString());
                    }
                    catch { }

                    if (pieceIndex == 34 && turboAvailable)
                    {
                        turboAvailable = false;
                        isTurbo = true;
                        Console.Title = "Team Ezimr HWO 2014";
                        send(new Turbo("GOTTA GO FASTE [SANIC INTENSIFIES]"));
                        break;
                    }

                    /*if(pieceIndex == 4 || pieceIndex == 19 )
                    {
                        send(new SwitchLane("Right"));
                        break;
                    }
                    else if(pieceIndex == 9)
                    {
                        send(new SwitchLane("Left"));
                        break;
                    }*/

                    
                        

                    throttle = Math.Abs(angle) >= 12.5 ? 0.95 : throttle;
                    throttle = Math.Abs(angle) >= 25 ? 0.7 : throttle;
                    throttle = Math.Abs(angle) >= 37.5 ? 0.5 : throttle;
                    throttle = Math.Abs(angle) >= 50 ? 0.2 : throttle;
					throttle = Math.Abs(angle) >= 55 ? 0.1 : throttle;
                    
                    if (pieceIndex == 31 || pieceIndex == 13 || pieceIndex == 14 || pieceIndex == 3 || pieceIndex == 26)
                        throttle = 0.0;
                    Console.Clear();
                    HWO_Log.Log("Car angle: {0}", angle);
                    HWO_Log.Log("Current lap: {0}/3", currentLap+1);
                    HWO_Log.Log("Piece: {0}", pieceIndex);
                    HWO_Log.Log("Throttle: {0}", throttle);
                    HWO_Log.Log("Tick: {0}", gameTick);

                    if (isTurbo && gameTick + 30 >= gameTick)
                        throttle = 0.5;
                    else
                        isTurbo = false;

                    send(new Throttle(throttle));
                    break;
                case "join":
                    HWO_Log.Log("Joined");
                    send(new Ping());
                    break;
                case "crash":
                    HWO_Log.Log("CRASHED!");
                    return;
                    break;
                case "gameInit":
                    HWO_Log.Log("Race init");
                    send(new Ping());
                    break;
                case "gameEnd":
                    HWO_Log.Log("Race ended");
                    send(new Ping());
                    break;
                case "gameStart":
                    HWO_Log.Log("Race starts");
                    send(new Ping());
                    break;
                default:
					HWO_Log.Log(msg.msgType);
					if(msg.msgType == "error")
						HWO_Log.Log(line);
                    send(new Ping());
                    break;
            }
        }
    }

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class Join : SendMsg
{
    public string name;
    public string key;
    public string color;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
        this.color = "red";
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}

class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(string value)
    {
        this.value = value;
    }

    protected override object MsgData()
    {
        return this.value;
    }
    protected override string MsgType()
    {
        return "switchLane";
    }
}

class Turbo : SendMsg
{
    public string value;

    public Turbo(string value = "")
    {
        this.value = value;
    }

    protected override object MsgData()
    {
        return this.value;
    }
    protected override string MsgType()
    {
        return "turbo";
    }
}

class HWO_Log
{
	public static void Log(string msg)
	{
		new WebClient().DownloadData("http://easimer.tk/hwo_log/send.php?date=" + DateTime.Now.ToString() + "&msg=" + msg);
	}
}